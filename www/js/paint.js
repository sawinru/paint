var Paint = function (context) {
	this.context	= context;
	this.type		= '';
	this.width		= 0;
	this.height		= 0;

	this.storage = {
		arc		: [],
		segment	: [],
		polyline: []
	};

	/**
	 * Выбранный элемент
	 * @type {null}
	 */
	this.select	= [];

	/**
	 * Выделение объекта
	 */
	this.selectObj = function (type, index) {
		if(this.storage[type] && this.storage[type][index]) {
			this.select.push({type: type, index: index});
			this.storage[type][index].color = 'red';

			this.draw();
		}
	};

	/**
	 * Убираем выделение
	 */
	this.unSelectObj = function () {
		if (this.select.length) {
			for (var i = 0; i < this.select.length; i++) {
				if (
					this.storage[this.select[i].type] &&
					this.storage[this.select[i].type][this.select[i].index]
				) {
					this.storage[this.select[i].type][this.select[i].index].color = 'black';
				}
			}
		}

		this.select = [];
		this.draw();
	};

	/**
	 * Чистит холст
	 */
	this.clear = function () {
		this.context.clearRect(0, 0, this.width, this.height);
	};

	/**
	 * Удаление выбранного/переданного элемента
	 * @param type
	 * @param index
	 */
	this.remove = function (type, index) {
		// удаление выбранных элементов
		if (this.select.length) {
			for (var i = 0; i < this.select.length; i++) {
				//this.storage[this.select[i].type].splice(this.select[i].index, 1);
				delete this.storage[this.select[i].type][this.select[i].index];
			}

			this.select = [];
		}

		this.draw();
	};

	/**
	 * Чистит холст и наносит все сохраненные объекты
	 */
	this.draw = function () {
		// чистим холст
		this.clear();

		/**
		 * Проходим по массиву с объектами
		 */
		var x = 0,
			i = 0;


		for (x in this.storage) {
			if (this.storage[x].length) {

				switch (x) {
					// строим окружности
					case 'arc': {
						for (i in this.storage[x]) {
							if (typeof this.storage[x] != 'object') { continue;}

							this.context.beginPath();
							this.context.strokeStyle = this.storage[x][i].color;

							this.context.arc(
								this.storage[x][i].x,
								this.storage[x][i].y,
								this.storage[x][i].radius,
								this.storage[x][i].startAngle,
								this.storage[x][i].endAngle,
								this.storage[x][i].ccw
							);
							if (this.storage[x][i].ccw) {
								this.context.closePath();
							}
							this.context.stroke();
						}

						break;
					}
					// строим отрезки
					case 'segment': {

						for (i in this.storage[x]) {
							if (typeof this.storage[x] != 'object') { continue;}

							this.context.beginPath();
							this.context.strokeStyle = this.storage[x][i].color;
							this.context.moveTo(this.storage[x][i].point1.x, this.storage[x][i].point1.y);
							this.context.lineTo(this.storage[x][i].point2.x, this.storage[x][i].point2.y);
							this.context.closePath();
							this.context.stroke();
						}
						break;
					}
					// строим полилинию
					case 'polyline': {

						for (i in this.storage[x]) {
							if (typeof this.storage[x] != 'object') { continue;}

							this.context.beginPath();
							this.context.strokeStyle = this.storage[x][i].color;
							for (var j in this.storage[x][i].pointList) {
								if (j > 0) {
									this.context.moveTo(this.storage[x][i].pointList[j-1].x, this.storage[x][i].pointList[j-1].y);
									this.context.lineTo(this.storage[x][i].pointList[j].x, this.storage[x][i].pointList[j].y);
								}
							}
							this.context.closePath();
							this.context.stroke();
						}
						break;
					}
				}
			}
		}
	};

	/**
	 * Построение окружности
	 * @param center
	 * @param point
	 */
	this.addCircle = function (center, point, color) {
		color = 'black';

		this.storage.arc.push({
			x			: center.x,
			y			: center.y,
			radius		: Math.sqrt(Math.pow((point.x - center.x), 2) + Math.pow((point.y - center.y), 2)),
			startAngle	: 0,
			endAngle	: Math.PI*2,
			ccw			: true,
			color		: color
		});

		this.draw();
	};

	/**
	 * Построение отрезка
	 * @param point1
	 * @param point2
	 */
	this.addSegment = function (point1, point2, color) {
		color = 'black';

		this.storage.segment.push({
			point1	: point1,
			point2	: point2,
			color	: color
		});

		this.draw();
	};

	this.addPolyline = function (pointList, color, index) {
		color = 'black';

		if (index != undefined && this.storage.polyline[index]) {
			this.storage.polyline[index] = {pointList: pointList, color: color};
		} else {
			this.storage.polyline.push({pointList: pointList, color: color});
		}

		this.draw();
	};

	/**
	 * Построение дуги
	 * @param center
	 * @param point1
	 * @param point2
	 */
	this.addArc = function (center, point1, point2, color) {
		color = 'black';

		var sAngle = this.getAngle(
			{x: center.x, y: center.y},
			{x: point1.x, y: point1.y}
		);
		var eAngle = this.getAngle(
			{x: center.x, y: center.y},
			{x: point2.x, y: point2.y}
		);

		this.storage.arc.push({
			x			: center.x,
			y			: center.y,
			radius		: Math.sqrt(Math.pow((point1.x - center.x), 2) + Math.pow((point1.y - center.y), 2)),
			startAngle	: sAngle,
			endAngle	: eAngle,
			ccw			: false,
			color		: color
		});

		this.draw();
	};

	/**
	 * Рассчитываем угол
	 * @param center
	 * @param point
	 * @returns {number}
	 */
	this.getAngle = function(center, point){
		var x = point.x - center.x;
		var y = point.y - center.y;
		if(x==0) return (y>0) ? Math.PI/2 : Math.PI*1.5;
		var a = Math.atan(y/x);
		a = (x > 0) ? a : a+Math.PI;
		return a;
	};
};


$(document).ready(function(){
	var $removeBtn	= $('[data-action="remove"]');
	var $eBtn       = $('[data-action="equidistant"]');
	var canvas	= document.getElementById('canvas');
	var context	= canvas.getContext('2d');
	var point	= [],
		eStep   = 10, // шаг эквидистанты
		polylineIndex = 0;

	canvas.width		= 1000;
	canvas.height		= 500;
	context.lineWidth	= 1;
	var offset		= $(canvas).offset();

	window.paint	= new Paint(context);
	paint.width		= canvas.width;
	paint.height	= canvas.height;

	var startSelect = null;

	$('[data-action="change-type"]').on('click', function () {
		window.paint.type = $(this).data('type');
		point = [];
		/**
		 * Меняем индекс полилинии, для создания новой
		 */
		polylineIndex = paint.storage.polyline.length;
	});

	/**
	 * Удаление выбранного элемента
	 */
	$removeBtn.on('click', function () {
		paint.remove();
		$(this).prop('disabled', true);
	});

	$eBtn.on('click', function () {
		/**
		 * Если есть выделенные объекты
		 */
		if (paint.select.length) {
			for (var i in paint.select) {
				if (paint.select[i].type == 'polyline') {
					context.beginPath();
					context.strokeStyle = 'green';

					for (var j in paint.storage[paint.select[i].type][paint.select[i].index].pointList) {
						if (j > 0) {
							var x1 = paint.storage[paint.select[i].type][paint.select[i].index].pointList[j-1].x,
								y1 = paint.storage[paint.select[i].type][paint.select[i].index].pointList[j-1].y + eStep,
								x2 = paint.storage[paint.select[i].type][paint.select[i].index].pointList[j].x,
								y2 = paint.storage[paint.select[i].type][paint.select[i].index].pointList[j].y + eStep;

							context.moveTo(x1, y1);
							context.lineTo(x2, y2);
						}
					}
					context.closePath();
					context.stroke();
				}
			}
		}
	});


	/**
	 * Отрисовка элементов
	 */
	$(canvas).on('click', function(pos) {
//		paint.unSelectObj();
//		$removeBtn.prop('disabled', true);

		var x = pos.clientX - offset.left,
			y = pos.clientY - offset.top;


		switch (paint.type) {
			case 'segment': {
				if (point.length == 1) {
					paint.addSegment({x: point[0][0], y:point[0][1]}, {x:x,y:y});
					point = [];
				} else {
					point.push([x,y]);
				}
				break;
			}
			case 'polyline': {
				point.push({x:x, y:y});

				if (point.length >= 2) {
					paint.addPolyline(point, 'black', polylineIndex);
				}
				break;
			}
			case 'circle': {
				if (point.length == 1) {
					paint.addCircle({x: point[0][0], y:point[0][1]}, {x:x,y:y});
					point = [];
				} else {
					point.push([x,y]);
				}
				break;
			}
			case 'arc': {
				if (point.length == 2) {
					paint.addArc(
						{x: point[0][0], y: point[0][1]},
						{x: point[1][0], y: point[1][1]},
						{x: x,y: y}
					);
					point = [];
				} else {
					point.push([x,y]);
				}
				break;
			}

			case 'select': {
				//var mouseX = (pos.pageX - offset.left);
				//var mouseY = (pos.pageY - offset.top);

				/**
				 * проверяем, в какой объект мы попали
				 */

				for (var i = 0; i < window.paint.storage.arc.length; i++) {
					var circleX	= window.paint.storage.arc[i].x;
					var circleY	= window.paint.storage.arc[i].y;
					var radius	= window.paint.storage.arc[i].radius;

					/**
					 * Проверяем, попали ли мы в область круга
					 */
					if (Math.pow(x - circleX, 2) + Math.pow(y - circleY, 2) < Math.pow(radius, 2)) {
						paint.selectObj('arc', i);
						$removeBtn.prop('disabled', false);
						return false;
					}
				}

				for (var i = 0; i < window.paint.storage.segment.length; i++) {
					var x1 = window.paint.storage.segment[i].point1.x,
						x2 = window.paint.storage.segment[i].point2.x,
						y1 = window.paint.storage.segment[i].point1.y,
						y2 = window.paint.storage.segment[i].point2.y;

					var a	= (y1-y2)/(x1-x2);
					var b	= ((y1+y2)-a*(x1+x2))/2;

					if (
						(y == a*x+b && x > x1 && x < x2) ||
						(y == a*x+b && x2 > x1 && x < x1)
					) {
						paint.selectObj('segment', i);
						$removeBtn.prop('disabled', false);
						return false;
					}
				}

				for (var i = 0; i < window.paint.storage.polyline.length; i++) {
					for (var j in window.paint.storage.polyline[i].pointList) {
						if (j > 0) {
							var x1 = window.paint.storage.polyline[i].pointList[j-1].x,
								x2 = window.paint.storage.polyline[i].pointList[j].x,
								y1 = window.paint.storage.polyline[i].pointList[j-1].y,
								y2 = window.paint.storage.polyline[i].pointList[j].y;

							var a	= (y1-y2)/(x1-x2);
							var b	= ((y1+y2)-a*(x1+x2))/2;

							if (
								(y == a*x+b && x > x1 && x < x2) ||
								(y == a*x+b && x2 > x1 && x < x1)
							) {
								paint.selectObj('polyline', i);
								$removeBtn.prop('disabled', false);
								return false;
							}
						}
					}
				}
				break;
			}
		}
	}.bind(this));


	/**
	 * Выбор элементов
	 */

	// действия при отпуске кнопки нажатия
	$(canvas).mouseup(function(e) {
		if (startSelect) {
			var clientX = e.clientX - offset.left,
				clientY = e.clientY - offset.top;


			var x1 = (startSelect.x < clientX) ? startSelect.x : clientX;
			var x2 = (startSelect.x < clientX) ? clientX : startSelect.x;
			var y1 = (startSelect.y < clientY) ? startSelect.y : clientY;
			var y2 = (startSelect.y < clientY) ? clientY : startSelect.y;

			/**
			 * Смотрим, в какой элемент попали
			 */
			for (var x in paint.storage) {
				if (paint.storage[x].length) {

					switch (x) {
						// строим окружности
						case 'arc': {
							for (var i in paint.storage[x]) {
								if (
									paint.storage[x][i].x >= x1 &&
									paint.storage[x][i].x <= x2 &&
									paint.storage[x][i].y >= y1 &&
									paint.storage[x][i].y <= y2
								) {
									paint.selectObj(x, i);
									$removeBtn.prop('disabled', false);
								}
							}

							break;
						}
						// смотрим отрезки
						case 'segment': {

							for (var i in paint.storage[x]) {
								if (
									(
										paint.storage[x][i].point1.x >= x1 &&
										paint.storage[x][i].point1.x <= x2 &&
										paint.storage[x][i].point1.y >= y1 &&
										paint.storage[x][i].point1.y <= y2
									) ||
									(
										paint.storage[x][i].point2.x >= x1 &&
										paint.storage[x][i].point2.x <= x2 &&
										paint.storage[x][i].point2.y >= y1 &&
										paint.storage[x][i].point2.y <= y2
									)
								) {
									paint.selectObj(x, i);
									$removeBtn.prop('disabled', false);
								}
							}
							break;
						}
						// строим полилинию
						case 'polyline': {

							for (i in paint.storage[x]) {
								for (var j in paint.storage[x][i].pointList) {
									if (
										paint.storage[x][i].pointList[j].x >= x1 &&
										paint.storage[x][i].pointList[j].x <= x2 &&
										paint.storage[x][i].pointList[j].y >= y1 &&
										paint.storage[x][i].pointList[j].y <= y2
									) {
										paint.selectObj(x, i);
										$removeBtn.prop('disabled', false);
									}
								}
							}
							break;
						}
					}
				}
			}


			startSelect = null;
		}
	});


	$(canvas).on('mousedown', function (e) {
		paint.unSelectObj();
		$removeBtn.prop('disabled', true);
		startSelect = {x: e.clientX - offset.left, y: e.clientY - offset.top};
	});


	$(canvas).on('mousemove', function (e) {
		if (startSelect) {
			paint.draw();

			var x = e.clientX - offset.left,
				y = e.clientY - offset.top;

			context.fillStyle = "rgba(100,150,185,0.5)";
			context.fillRect(startSelect.x, startSelect.y, x-startSelect.x, y-startSelect.y);
		}
	});

});
